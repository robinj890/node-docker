const knex = require('knex');

const db=knex({
  client: 'postgres',
  connection: {
    host: 'db',
    user: 'postgres',
    password: '12345',
    database: 'Grocery',
  },
});

(async () => {
    try {
      await db.schema.dropTableIfExists('product')
      await db.schema.withSchema('public').createTable('product', (table) => {
        table.increments('product_id').primary().notNullable()
        table.string('name').notNullable()
        table.string('category').notNullable()
        table.string('description').notNullable()
        table.integer('price').notNullable()
        table.integer('quantity').notNullable()
      })

      console.log('Created products table!')
      process.exit(0)
    } catch (err) {
      console.log(err)
      process.exit(1)
    }
  })();

