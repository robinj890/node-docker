const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json());
const { Pool } = require('pg');
const pool = new Pool({
    user: 'postgres',
    host: 'db',
    database: 'Grocery',
    password: '12345',
}
);

var user = {
    name: 'robin',
    age: 30,
    email: 'robin@gmail.com'
};



app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'DELETE, PUT');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    if ('OPTIONS' == req.method) {
        res.sendStatus(200);
    }
    else {
        next();
    }
});

app.get('/', function (req, res) {
    res.json(user);
    res.end();
});

app.get('/dummies', function (req, res) {
  
    (async () => {
     
        const client = await pool.connect()
        try {
          await client.query('BEGIN');
          const queryText = 'INSERT INTO product(name,category,description,price,quantity) VALUES($1,$2,$3,$4,$5)';
         await client.query(queryText, ["parle","biscuits","500gm",50,30]);
         await client.query(queryText, ["dove","soaps","100gm",60,20]);
         await client.query(queryText, ["maggi","noodles","5 pack",80,50]);
         
          await client.query('COMMIT');
        } catch (e) {
          await client.query('ROLLBACK');
          throw e
        } finally {
          client.release();
        }
      })().catch(e => console.error(e.stack));


    
    res.end();
});

app.get('/products', function (req, res) {


    pool.query('SELECT * from product', (err, result) => {

        if (err)
            console.log(err);
        else
        {
            result.rows.push({"product_id": 0,"name": "node","category": "node","description": "node","price": 0,"quantity": 0});
            res.json(result.rows);
            
        }
        res.end();

    });
});


app.post('/add', function (req, res) {
    var data=req.body;
    
    (async () => {
     
        const client = await pool.connect()
        try {
          await client.query('BEGIN');
          const queryText = 'INSERT INTO product(name,category,description,price,quantity) VALUES($1,$2,$3,$4,$5)';
         await client.query(queryText, [data.name,data.category,data.description,+data.price,+data.quantity]);
         
          await client.query('COMMIT');
        } catch (e) {
          await client.query('ROLLBACK');
          throw e
        } finally {
          client.release();
        }
      })().catch(e => console.error(e.stack));


    
    res.end();
});



app.listen(8085);
console.log('port:8085');

